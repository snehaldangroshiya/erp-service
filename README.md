this project is inspired from terraform https://www.terraform.io/.

Dependency managements
=======================
- To manage the dependency we use https://github.com/golang/dep
    - execute this command to install dep
       ```
       go get github.com/golang/dep
       go to ERP_v4 folder
       dep ensure
       dep status
       ```
- Use ```make mac``` to build executable for mac os
- Use ```make linux``` to build executable for linux
- Use ```make stop``` to stop server

Folder structure 
================
```
Go
|___src
     |___ github.com
            |___ ERP_v4
                  |___ builtin - It should contains the default plugins
                  |___ command - Cli command options
                  |___ erp     - System specific code
                  |___ plugin  - Plugin architeture related code
                  |___ schema  - Plugin schema related code.
                  |___ vendor  - It is containing dependency packages.
```

Authentication done by two ways
==============================
    1. LDAP
    2. DB (mongoDB) - currently working on this

   LDAP
   ----
   - To authenticate user by LDAP we requires to set LDAP_SERVER environment variable.
   - which contains the LDAP server IP address. for linux and MAC os
    e.g. 
    ``` export LDAP_SERVER=10.0.0.13 ```

   DB (Not Recommended as of now)
   ------------------------------
    - It will simply create the new user entry in mongoDB database.

- Future implementation idea and research work is written in ERP_v4/research file
Note : they are just rough idea also not in full details


TODO
 - install dependencies by using Makefile
 - prepare separate interface for backend
   which handle the authentication part as well as
   LDAP and mongoDB related functions.
 - write initial unit test for the working modules
 - add/improve comments
 - prepare doc by using makefile
