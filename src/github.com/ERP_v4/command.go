package main

import (
	"os"

	"github.com/ERP_v4/command"
	"github.com/mitchellh/cli"
)

// Commands is the mapping of all the available erp commands.
var Commands map[string]cli.CommandFactory

// Ui is the cli.Ui used for communicating to the outside world.
var Ui cli.Ui

func init() {

	Ui = &cli.ColoredUi{
		InfoColor:   cli.UiColor{Code: cli.UiColorYellow.Code, Bold: true},
		ErrorColor:  cli.UiColorRed,
		WarnColor:   cli.UiColorCyan,
		OutputColor: cli.UiColorGreen,
		Ui:          &cli.BasicUi{Writer: os.Stdout},
	}

	meta := command.Meta{
		ContextOpts: &ContextOpts,
		Ui:          Ui,
	}

	Commands = map[string]cli.CommandFactory{
		"internal-plugin": func() (cli.Command, error) {
			return &command.InternalPluginCommand{
				Meta: meta,
			}, nil
		},
		"rest-server": func() (cli.Command, error) {
			return &command.RESTCommandFactory{
				Meta: meta,
			}, nil
		},
		"sync": func() (cli.Command, error) {
			return &command.SyncDB{
				Meta: meta,
			}, nil
		},
		"user": func() (cli.Command, error) {
			return &command.User{
				Meta: meta,
			}, nil
		},
	}
}
