package command

import (
	"strings"

	"github.com/ERP_v4/erp/provider"
)

type SyncDB struct {
	Meta
}

func (c *SyncDB) Run(args []string) int {
	c.Meta.Ui.Info("Synchronization started...!!!!")
	provider.InitProvider()
	c.Meta.Ui.Info("Synchronization completed...")
	return 0
}

func (c *SyncDB) Help() string {
	helpText := `
Usage: erp sync [options] [DIR]
	Synchronize all LDAP users with MongoDB
`
	return strings.TrimSpace(helpText)
}

func (c *SyncDB) Synopsis() string {
	return "Synchronize LDAP users with MongoDB"
}
