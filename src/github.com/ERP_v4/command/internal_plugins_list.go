package command

import (
	auth "github.com/ERP_v4/builtin/providers/authentication"
	"github.com/ERP_v4/builtin/providers/ems"
	hierarchy "github.com/ERP_v4/builtin/providers/hierarchy"
	"github.com/ERP_v4/plugin"
)

var InternalProviders = map[string]plugin.ProviderFunc{
	"auth":      auth.Provider,
	"hierarchy": hierarchy.Provider,
	"ems":       ems.Provider,
}
