package command

import (
	"encoding/json"
	"log"
	"net/http"
	"strings"

	"github.com/ERP_v4/erp"
	"github.com/gorilla/mux"
)

type RESTCommandFactory struct {
	Meta
}

func (c *RESTCommandFactory) Run(args []string) int {
	c.Ui.Info("Server is started on port:8000...")
	router := mux.NewRouter()
	router.HandleFunc("/ERP", c.handler)
	router.PathPrefix("/").Handler(http.FileServer(http.Dir("../../../../")))
	log.Fatalln(http.ListenAndServe(":8000", router))
	return 0
}

func (c *RESTCommandFactory) handler(w http.ResponseWriter, r *http.Request) {
	log.Println("[LOG 1]: Triggered REST handler")
	config, err := erp.LoadJsonRequest(r.Body)
	if err != nil {
		log.Println("[ERROR] Request is not in proper formate")
	}

	ctx, err := c.Context(&contextOpts{
		Config: config,
	})

	if err != nil {
		log.Println("[ERROR] Context object not generated")
	}
	log.Println("[LOG 3]: Execute function context is called ")
	ctx.Execute()

	response := ctx.Output()
	w.Header().Set("Access-Control-Allow-Methods", "POST")
	w.Header().Set("Access-Control-Allow-Headers", "Accept, Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization")
	w.Header().Set("Content-Type", "application/json")

	json.NewEncoder(w).Encode(response)
}

func (c *RESTCommandFactory) Help() string {
	helpText := `
Usage: erp fmt [options] [DIR]

	Rewrites all erp configuration files to a canonical format.

`
	return strings.TrimSpace(helpText)
}

func (c *RESTCommandFactory) Synopsis() string {
	return "REST server for the ERP system"
}
