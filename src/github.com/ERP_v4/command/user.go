package command

import (
	"os"
	"strings"

	"github.com/ERP_v4/erp/core"
	"github.com/ERP_v4/erp/mongo"
	"github.com/ERP_v4/erp/provider"
)

type User struct {
	Meta
}

func (c *User) Run(args []string) int {
	cmdFlag := c.Meta.flagSet("user")
	cmdFlag.Usage = func() {
		c.Ui.Error(c.Help())
	}
	if err := cmdFlag.Parse(os.Args); err != nil {
		cmdFlag.Usage()
	}
	switch cmdFlag.Arg(2) {
	case "create":
		c.create()
		break
	case "delete":
		c.delete()
		break
	case "update":
		break
	case "read":
		break
	default:
		cmdFlag.Usage()
	}
	return 0
}

func (c *User) delete() int {
	delete := c.Meta.flagSet("delete")
	userId := delete.String("u", "", "Unique user id")
	if er := delete.Parse(os.Args[:3]); er != nil {
		delete.PrintDefaults()
	}
	if len(*userId) == 0 {
		delete.PrintDefaults()
		return 0
	}
	user := mongo.GetUserDetails(*userId, "")
	if user.IsExist() == false {
		c.Ui.Error("User is not exist please.")
		return 0
	}
	return 0
}

func (c *User) create() int {
	create := c.Meta.flagSet("create")
	uname := create.String("u", "", "user id for the new user.")
	passwd := create.String("p", "", "user password.")
	fName := create.String("f", "", "user full name.")
	if er := create.Parse(os.Args[3:]); er != nil {
		create.PrintDefaults()
	}

	if len(*uname) == 0 || len(*passwd) == 0 || len(*fName) == 0 {
		create.PrintDefaults()
		return 0
	}

	type createUser struct {
		PR_DISPLAY_NAME string `bson:"display_name,omitempty"`
		PR_PASSWORD     string `bson:"password,omitempty"`
		PR_USER_NAME    string `bson:"user_name,omitempty"`
	}

	userInfo := &createUser{
		PR_DISPLAY_NAME: *fName,
		PR_PASSWORD:     *passwd,
		PR_USER_NAME:    *uname,
	}

	user := mongo.GetUserDetails(userInfo.PR_USER_NAME, userInfo.PR_PASSWORD)
	if user.IsExist() == true {
		c.Ui.Error("User is already exist please use different user name.")
		return 0
	}

	if err := mongo.CreateDocument(core.DB_USER_TABLE, userInfo); err != nil {
		c.Ui.Error("[Error]: While creating user in database")
	}

	if isCollectionExist := mongo.IsCollectionExist(core.DB_USER_TABLE); isCollectionExist == false {
		c.Ui.Error("[Error] Collection is not exist in database.")
		return 0
	}

	provider.CreateMessageStore(userInfo.PR_DISPLAY_NAME)
	return 0
}

func (c *User) Help() string {
	helpText := `
Usage: erp user [options]

	Available Command:
	create  Create new user in database
	Delete  Delete user from database
	Update  Update user information in database
	Read    Read user information from database
	`
	return strings.TrimSpace(helpText)
}

func (c *User) HelpCreate() string {
	helpText := `
Usage: erp user create [options]

	Flags:
	-u    userID     User id for user.
	-p    password   Password for user.
	-f    FullName   Full name for user.
	`
	return strings.TrimSpace(helpText)
}

func (c *User) Synopsis() string {
	return "User Management."
}
