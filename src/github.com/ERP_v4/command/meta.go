package command

import (
	"log"

	"flag"

	"github.com/ERP_v4/erp"
	"github.com/mitchellh/cli"
)

// Meta are the meta-options that are available on all or most commands.
type Meta struct {
	ContextOpts *erp.ContextOpts
	Ui          cli.Ui
}

// Context returns a erp Context taking into account the context
// options used to initialize this meta configuration.
func (m *Meta) Context(configOpt *contextOpts) (*erp.Context, error) {
	opts := m.contextOpts()
	config := *configOpt.Config
	opts.ProviderConfig = *config.ProviderConfigs
	opts.Resource = *config.Resources
	ctx, err := erp.NewContext(opts)
	log.Println("[LOG 2]: Context created")
	return ctx, err
}

// contextOpts returns the options to use to initialize a erp
// context with the settings from this Meta.
func (m *Meta) contextOpts() *erp.ContextOpts {
	var opts erp.ContextOpts = *m.ContextOpts
	return &opts
}

// Context options contains the request information
// which received from client. contextOpts are the
// options used to load a context from a command.
type contextOpts struct {
	Config *erp.Config
}

// flags adds the meta flags to the given FlagSet.
func (m *Meta) flagSet(n string) *flag.FlagSet {
	f := flag.NewFlagSet(n, flag.ContinueOnError)
	return f
}
