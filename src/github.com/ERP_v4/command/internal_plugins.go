package command

import (
	"fmt"
	"log"
	"strings"

	"github.com/ERP_v4/plugin"
	"github.com/kardianos/osext"
)

type InternalPluginCommand struct {
	Meta
}

const SPACE = "-SPACE-"

// BuildPluginCommandString builds a special string for executing internal
// plugins. It has the following format:
//
// 	/path/to/erp-SPACE-internal-plugin-SPACE-erp-provider-aws
//
// We split the string on -SPACE- to build the command executor. The reason we
// use -SPACE- is so we can support spaces in the /path/to/erp part.
func BuildPluginCommandString(pluginType, pluginName string) (string, error) {
	erpPath, err := osext.Executable()
	if err != nil {
		return "", err
	}
	parts := []string{erpPath, "internal-plugin", pluginType, pluginName}
	return strings.Join(parts, SPACE), nil
}

func (c *InternalPluginCommand) Run(args []string) int {
	if len(args) != 2 {
		log.Printf("Wrong number of args; expected: erp internal-plugin pluginType pluginName")
		return 1
	}

	pluginType := args[0]
	pluginName := args[1]

	log.SetPrefix(fmt.Sprintf("%s-%s (internal) ", pluginName, pluginType))
	switch pluginType {
	case "provider":
		pluginFunc, found := InternalProviders[pluginName]
		if !found {
			log.Printf("[ERROR] Could not load provider: %s", pluginName)
			return 1
		}
		log.Printf("[INFO] Starting provider plugin %s", pluginName)
		plugin.Serve(&plugin.ServeOpts{
			ProviderFunc: pluginFunc,
		})
	default:
		log.Printf("[ERROR] Invalid plugin type %s", pluginType)
		return 1
	}
	return 0
}

func (c *InternalPluginCommand) Help() string {
	helpText := `
Usage: erp fmt [options] [DIR]

	Rewrites all erp configuration files to a canonical format.

`
	return strings.TrimSpace(helpText)
}

func (c *InternalPluginCommand) Synopsis() string {
	return "RPC server for the ERP system"
}
