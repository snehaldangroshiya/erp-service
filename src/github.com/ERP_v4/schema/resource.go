package schema

import (
	"fmt"
	"log"

	"github.com/ERP_v4/erp"
)

// Resource represents a thing in erp that has a set of configurable
// attributes and a lifecycle (create, read, update, delete).
//
// The Resource schema is an abstraction that allows provider writers to
// worry only about CRUD operations while off-loading validation, diff
// generation, etc. to this higher level library.
//
// In spite of the name, this struct is not used only for erp resources,
// but also for data sources. In the case of data sources, the Create,
// Update and Delete functions must not be provided.
type Resource struct {

	// Schema is the schema for the configuration of this resource.
	//
	// The keys of this map are the configuration keys, and the values
	// describe the schema of the configuration value.
	//
	// The schema is used to represent both configurable data as well
	// as data that might be computed in the process of creating this
	// resource.
	Schema map[string]*Schema

	// The functions below are the CRUD operations for this resource.
	//
	// The only optional operation is Update. If Update is not implemented,
	// then updates will not be supported for this resource.
	//
	// The ResourceData parameter in the functions below are used to
	// query configuration and changes for the resource as well as to set
	// the ID, computed data, etc.
	//
	// The interface{} parameter is the result of the ConfigureFunc in
	// the provider for this resource. If the provider does not define
	// a ConfigureFunc, this will be nil. This parameter should be used
	// to store API clients, configuration structures, etc.
	//
	// If any errors occur during each of the operation, an error should be
	// returned. If a resource was partially updated, be careful to enable
	// partial state mode for ResourceData and use it accordingly.
	//
	// Exists is a function that is called to check if a resource still
	// exists. If this returns false, then this will affect the diff
	// accordingly. If this function isn't set, it will not be called. It
	// is highly recommended to set it. The *ResourceData passed to Exists
	// should _not_ be modified.
	Create CreateFunc
	Read   ReadFunc
	Update UpdateFunc
	Delete DeleteFunc
	Exists ExistsFunc
}

// See Resource documentation.
type CreateFunc func(*ResourceData, interface{}) error

// See Resource documentation.
type ReadFunc func(*ResourceData, interface{}) error

// See Resource documentation.
type UpdateFunc func(*ResourceData, interface{}) error

// See Resource documentation.
type DeleteFunc func(*ResourceData, interface{}) error

// See Resource documentation.
type ExistsFunc func(*ResourceData, interface{}) (bool, error)

// function which is used to handle the Item request. item request
// are further categorized by the different actions like save/update,read/open,delete
//
// Based on the different action type resource triggers the resource handlers.
func (r *Resource) Item(re *erp.Resource, meta interface{}) *erp.Output {
	log.Println("[Log 10]: Triggered the resource action function")
	return r.doAction("items", re, meta)
}

// function which is used to handle the List request. list request
// are further categorized by the different actions like read/open,delete
//
// Based on the different action type resource triggers the resource handlers.
func (r *Resource) List(re *erp.Resource, meta interface{}) *erp.Output {
	return r.doAction("list", re, meta)
}

func (r *Resource) doAction(action string, re *erp.Resource, meta interface{}) *erp.Output {
	itemsData := re.RawConfig.Get(action).([]map[string]interface{})
	data, _ := schemaMap(r.Schema).Data(re)

	for _, raw := range itemsData {
		data.config = raw
		switch re.Action {
		case "read", "open":
			r.Read(data, meta)
			break
		case "save":
			if data.Get("entryid") == nil {
				r.Create(data, meta)
			} else {
				if r.Update == nil {
					fmt.Errorf("doesn't support update")
				}
				r.Update(data, meta)
			}
			break
		case "delete":
			r.Delete(data, meta)
			break
		}
	}

	return data.newState
}
