package schema

import (
	"sync"

	"github.com/ERP_v4/erp"
)

// ResourceData is used to query and set the attributes of a resource.
//
// ResourceData is the primary argument received for CRUD operations on
// a resource as well as configuration of a provider. It is a powerful
// structure that can be used to not only query data, but check for changes,
// define partial state updates, etc.
//
// The most relevant methods to take a look at are Get, Set, and Partial.
type ResourceData struct {
	// Settable (internally)
	schema map[string]*Schema
	config map[string]interface{}
	raw    *erp.RawConfig
	meta   map[string]string

	// Don't set
	// Create multi level stack which call for each request.
	// idea behind create stack is to achieve cyclic checking
	// for the each request like user authentication, prepare proper
	// data structure from request date, Error handling while processing request
	// and prepare proper response.
	message_action []map[string]interface{}
	newState       *erp.Output
	once           sync.Once
	isNew          bool
}

func (d *ResourceData) MarkNewResource() {
	d.isNew = true
}

func (d *ResourceData) IsNewResource() bool {
	return d.isNew
}

// Function which is used to get the properties from config data
// in case if we don't get the property we try to find that from RowConfigs
// which contains full request data.
func (d *ResourceData) Get(key string) interface{} {
	value := d.config[key]
	if value != nil {
		return value
	}
	sch_attribute := d.schema[key]
	value, _ = sch_attribute.DefaultValue()
	return value
}

func (d *ResourceData) Set(value *erp.Output) {
	d.newState = value
}
