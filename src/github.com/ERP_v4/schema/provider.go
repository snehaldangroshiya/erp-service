package schema

import (
	"github.com/ERP_v4/erp"
)

// Provider represents a resource provider in erp, and properly
// implements all of the ResourceProvider API.
//
// By defining a schema for the configuration of the provider, the
// map of supporting resources, and a configuration function, the schema
// framework takes over and handles all the provider operations for you.
//
// After defining the provider structure, it is unlikely that you'll require any
// of the methods on Provider itself.
type Provider struct {
	// Schema is the schema for the configuration of this provider. If this
	// provider has no configuration, this can be omitted.
	//
	// The keys of this map are the configuration keys, and the value is
	// the schema describing the value of the configuration.
	Schema map[string]*Schema

	// ResourcesMap is the list of available resources that this provider
	// can manage, along with their Resource structure defining their
	// own schemas and CRUD operations.
	//
	// Provider automatically handles routing operations such as Apply,
	// Diff, etc. to the proper resource.
	ResourcesMap map[string]*Resource

	// ConfigureFunc is a function for configuring the provider. If the
	// provider doesn't need to be configured, this can be omitted.
	//
	// See the ConfigureFunc documentation for more information.
	ConfigureFunc ConfigureFunc
	meta          interface{}
}

// ConfigureFunc is the function used to configure a Provider.
//
// The interface{} value returned by this function is stored and passed into
// the subsequent resources as the meta parameter. This return value is
// usually used to pass along a configured API client, a configuration
// structure, etc.
type ConfigureFunc func(*ResourceData) (interface{}, error)

// Apply implementation of erp.ResourceProvider interface.
func (p *Provider) Item(r *erp.Resource) *erp.Output {
	resourceObj := p.ResourcesMap[r.Name]
	output := resourceObj.Item(r, p.meta)
	return output
}

// Apply implementation of erp.ResourceProvider interface.
func (p *Provider) List(r *erp.Resource) *erp.Output {
	resourceObj := p.ResourcesMap[r.Name]
	output := resourceObj.List(r, p.meta)
	return output
}
