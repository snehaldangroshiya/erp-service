package main

import (
	"fmt"
	"os"
	"os/exec"
	"strings"

	"github.com/ERP_v4/command"
	"github.com/ERP_v4/erp"
	erpPlugin "github.com/ERP_v4/plugin"
	"github.com/hashicorp/go-plugin"
	"github.com/mitchellh/cli"
)

// Config is the structure of the configuration for the erp CLI.
//
// This is not the configuration for erp itself. That is in the
// "config" package.
type Config struct {
	Providers map[string]string
}

var BuiltinConfig Config

// ContextOpts are the global ContextOpts we use to initialize the CLI.
var ContextOpts erp.ContextOpts

// Discover plugins located on disk, and fall back on plugins baked into the
// erp binary.
//
// We look in the following places for plugins:
//
// 1. Path where erp is invoked
//@ToDO 2. invoke plugin from the remote location.

// Whichever file is discoverd LAST wins.
//
// Finally, we look at the list of plugins compiled into erp. If any of
// them has not been found on disk we use the internal version. This allows
// users to add / replace plugins without recompiling the main binary.
func (c *Config) Discover(ui cli.Ui) error {
	c.Providers = make(map[string]string)
	// if we have a plugin compiled into erp and we didn't find
	// a replacement on disk, we'll just use the internal version. Only do this
	// from the main process, or the log output will break the plugin handshake.
	if os.Getenv("ERP_PLUGIN_MAGIC_COOKIE") == "" {
		for name, _ := range command.InternalProviders {
			if path, found := c.Providers[name]; found {
				// Allow these warnings to be suppressed via TF_PLUGIN_DEV=1 or similar
				if os.Getenv("ERP_PLUGIN_DEV") == "" {
					ui.Warn(fmt.Sprintf("[WARN] %s overrides an internal plugin for %s-provider.\n"+
						"  If you did not expect to see this message you will need to remove the old plugin.\n"+
						"  See https://www.erp.io/docs/internals/internal-plugins.html", path, name))
				}
			} else {
				cmd, err := command.BuildPluginCommandString("provider", name)
				if err != nil {
					return err
				}
				c.Providers[name] = cmd
			}
		}
	}

	return nil
}

// ProviderFactories returns the mapping of prefixes to
// ResourceProviderFactory that can be used to instantiate a
// binary-based plugin.
func (c *Config) ProviderFactories() map[string]erp.ResourceProviderFactory {
	result := make(map[string]erp.ResourceProviderFactory)
	for k, v := range c.Providers {
		result[k] = c.providerFactory(v)
	}

	return result
}

func (c *Config) providerFactory(path string) erp.ResourceProviderFactory {
	var config plugin.ClientConfig

	config.Cmd = pluginCmd(path)
	config.HandshakeConfig = erpPlugin.Handshake
	config.Managed = true
	config.Plugins = erpPlugin.PluginMap
	client := plugin.NewClient(&config)

	return func() (erp.ResourceProvider, error) {
		// Request the RPC client so we can get the provider
		// so we can build the actual RPC-implemented provider.
		rpcClient, err := client.Client()
		if err != nil {
			return nil, err
		}

		raw, err := rpcClient.Dispense(erpPlugin.ProviderPluginName)
		if err != nil {
			return nil, err
		}

		return raw.(erp.ResourceProvider), nil
	}
}

func pluginCmd(path string) *exec.Cmd {
	cmdPath := ""

	// No plugin binary found, so try to use an internal plugin.
	if strings.Contains(path, command.SPACE) {
		parts := strings.Split(path, command.SPACE)
		return exec.Command(parts[0], parts[1:]...)
	}

	// If we still don't have a path, then just set it to the original
	// given path.
	if cmdPath == "" {
		cmdPath = path
	}

	// Build the command to execute the plugin
	return exec.Command(cmdPath)
}
