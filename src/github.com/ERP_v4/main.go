package main

import (
	"fmt"
	"log"
	"os"

	"github.com/hashicorp/go-plugin"
	"github.com/mitchellh/cli"
)

func main() {
	// Override global prefix set by go-dynect during init()
	log.SetPrefix("ERP:")
	os.Exit(realMain())
}

func realMain() int {
	// Make sure we clean up any managed plugins at the end of this
	defer plugin.CleanupClients()

	//log.SetOutput(ioutil.Discard)
	config := BuiltinConfig

	if err := config.Discover(Ui); err != nil {
		Ui.Error(fmt.Sprintf("Error discovering plugins: %s", err))
		return 1
	}

	// Get the command line args. We shortcut "--version" and "-v" to
	// just show the version.
	args := os.Args[1:]
	for _, arg := range args {
		if arg == "-v" || arg == "-version" || arg == "--version" {
			newArgs := make([]string, len(args)+1)
			newArgs[0] = "version"
			copy(newArgs[1:], args)
			args = newArgs
			break
		}
	}
	cli := &cli.CLI{
		Args:       args,
		Commands:   Commands,
		HelpFunc:   helpFunc,
		HelpWriter: os.Stdout,
	}

	ContextOpts.Providers = config.ProviderFactories()

	exitCode, err := cli.Run()
	if err != nil {
		Ui.Error(fmt.Sprintf("Error executing CLI: %s", err.Error()))
		return 1
	}

	return exitCode
}
