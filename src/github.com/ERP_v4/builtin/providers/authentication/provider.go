package authentication

import (
	"github.com/ERP_v4/erp"
	"github.com/ERP_v4/schema"
)

// Provider returns a erp.ResourceProvider.
func Provider() erp.ResourceProvider {

	// The actual provider
	return &schema.Provider {
		ResourcesMap: map[string]*schema.Resource{
			"user_authentication": resourceAuthenticateUser(),
			"user_session": resourceUserSession(),
		},
	}
}
