package authentication

import (
	"github.com/ERP_v4/schema"
	"github.com/ERP_v4/erp"
	"github.com/ERP_v4/erp/provider"
	"log"
)

func resourceUserSession() *schema.Resource {
	return &schema.Resource {
		Read: resourceReadUserSession,
		Schema: map[string]*schema.Schema{
			"token": &schema.Schema{
				Type:         schema.TypeString,
				Optional:     true,
				Default:      nil,
			},
		},
	}
}

func resourceReadUserSession(d *schema.ResourceData, meta interface{}) error {
	token := d.Get("token").(string)

	if token, ok := d.Get("token").(string); ok == false {
		log.Println(token)
		panic(true)
	}
	var newState *erp.Output
	if token == ""{
		newState = &erp.Output{
			Success:false,
		}
	} else {
		var sessionActive = provider.IsSessionActives(token)
		if sessionActive == false {
			newState = &erp.Output{
				Success:sessionActive,
				Token:token,
				ErrorMsg:"Session is no more exist",
			}
		} else {
			newState = &erp.Output{
				Success:sessionActive,
				Token:token,
			}
		}
	}

	d.Set(newState)
	return nil
}