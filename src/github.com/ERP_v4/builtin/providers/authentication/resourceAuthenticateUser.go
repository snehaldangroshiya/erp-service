package authentication

import (
	"log"

	"os"

	"github.com/ERP_v4/erp"
	"github.com/ERP_v4/erp/core"
	"github.com/ERP_v4/erp/ldap"
	"github.com/ERP_v4/erp/mongo"
	"github.com/ERP_v4/erp/provider"
	"github.com/ERP_v4/schema"
	"github.com/satori/go.uuid"
	"gopkg.in/mgo.v2/bson"
)

func resourceAuthenticateUser() *schema.Resource {
	return &schema.Resource{
		Read: resourceReadAuthenticateUser,
		Schema: map[string]*schema.Schema{
			"username": &schema.Schema{
				Type:     schema.TypeString,
				Required: true,
			},
			"password": &schema.Schema{
				Type:     schema.TypeString,
				Optional: true,
			},
			"token": &schema.Schema{
				Type:     schema.TypeString,
				Optional: true,
				Default:  "",
			},
			"auth_by_ldap": &schema.Schema{
				Type:        schema.TypeBool,
				Optional:    true,
				DefaultFunc: defaultAuthType,
			},
		},
	}
}

func defaultAuthType() (interface{}, error) {
	return len(os.Getenv("LDAP_SERVER")) != 0, nil
}

func resourceReadAuthenticateUser(d *schema.ResourceData, meta interface{}) error {
	log.Println("[Log 11]: resourceAuthenticateUserRead function of resource called ")
	username := d.Get("username").(string)
	password := d.Get("password").(string)
	token := d.Get("token").(string)
	ldapAuth := d.Get("auth_by_ldap").(bool)
	if token == "" {
		var (
			isAuth      bool
			displayName string
		)
		if ldapAuth == true {
			ldapConfig := ldap.GetLDAP()
			ldapConfig.UserName = username
			ldapConfig.Password = password
			isAuth, displayName = ldapConfig.Authentication()
		} else {
			// TODO: not work properly don't try this please
			userDetails := mongo.GetUserDetails(username, password)
			isAuth, displayName = userDetails.Authentication()
		}
		var errorMsg string
		if isAuth == false {
			errorMsg = "Please check user credentials"
		} else {
			token = uuid.NewV4().String()
			// TODO: Create backend interface which used to insert, update, delete the
			// record from MongoDB database.
			props := erp.GetPropsStructs()
			props.PR_DISPLAY_NAME = displayName
			props.PR_SESSION_ID = token
			if error := mongo.CreateDocument(core.SESSIONS_TABLE, props); error != nil {
				log.Println("Error: Record is not properly inserted in", core.SESSIONS_TABLE)
			}

			var query interface{} = bson.M{"display_name": displayName}
			if msgStoreProps, e := mongo.Search(core.STORES_TABLE, query, 0, 1); e == nil {
				for _, v := range msgStoreProps {
					b, _ := bson.Marshal(v)
					bson.Unmarshal(b, props)
				}
			}

			provider.InitSession(token, props.Id.Hex())
		}
		newState := &erp.Output{
			Success:  isAuth,
			ErrorMsg: errorMsg,
			Token:    token,
		}
		d.Set(newState)
	} else {
		newState := &erp.Output{
			Success:  false,
			ErrorMsg: "Token is not exist",
		}
		d.Set(newState)
	}
	return nil
}
