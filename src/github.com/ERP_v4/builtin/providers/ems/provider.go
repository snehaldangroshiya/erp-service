package ems

import (
	"github.com/ERP_v4/erp"
	"github.com/ERP_v4/schema"
)

// Provider returns a erp.ResourceProvider.
func Provider() erp.ResourceProvider {

	// The actual provider
	return &schema.Provider{
		ResourcesMap: map[string]*schema.Resource{
			"ems_activate": resourceEmsModule(),
		},
	}
}

/**

what activate can do?
 - create new document in object collection which is
represent as a root folder for the ems plugin

 - ems has plugin function called getHierarchy function
which gives the sub project/plugin folder which in under then EMS
e.g.
EMS
|__Leave management
|__Payroll management
|__Document management
|__Employee detail management
|__ etc.....
{
	"provider" : "ems" ,
	"resource" : "ems_activate" ,
	"request_type" : "item" ,
	"action" : "read" ,
	"message_action" : {
		"action":"install"
	},
	"items" : [{
		"username" : "sd",
		"password" : "aa",
		"token" : ""
	}]
}


{
	"provider" : "ems" ,
	"request_type" : "list" ,
	"resource" : "ems_activate" ,
	"action" : "read" ,
	"token" : "4ccde64a-60c8-4371-aab9-15de152de78d"
}
*/
