package hierarchy

import (
	"github.com/ERP_v4/erp"
	"github.com/ERP_v4/erp/core"
	"github.com/ERP_v4/erp/mongo"
	"github.com/ERP_v4/erp/provider"
	"github.com/ERP_v4/schema"
	"gopkg.in/mgo.v2/bson"
)

func resourceHierarchyLoad() *schema.Resource {
	return &schema.Resource{
		Read: list_hierarchy,
		Schema: map[string]*schema.Schema{
			"token": &schema.Schema{
				Type:     schema.TypeString,
				Required: true,
				Default:  "",
			},
		},
	}
}

func list_hierarchy(d *schema.ResourceData, meta interface{}) error {
	token := d.Get("token").(string)
	if len(token) == 0 {
		newState := &erp.Output{
			Success:  true,
			ErrorMsg: "Token is not provided.",
		}
		d.Set(newState)
		return nil
	}

	type session_data struct {
		Display_name string `json:"display_name", omitempty`
	}

	var session = &session_data{}
	var query interface{} = bson.M{"session_id": token}
	if msgStoreProps, e := mongo.Search(core.SESSIONS_TABLE, query, 0, 1); e == nil {
		for _, v := range msgStoreProps {
			b, _ := bson.Marshal(v)
			bson.Unmarshal(b, session)
		}
	}

	if len(session.Display_name) == 0 {
		newState := &erp.Output{
			Success:  true,
			ErrorMsg: "Session is not exist.",
		}
		d.Set(newState)
		return nil
	}

	// Retrieve default message store from user name.
	messageStoreProps := provider.DefaultMessageStore(session.Display_name)
	var items []*erp.ListItem
	parent_entryid := messageStoreProps.PR_PARENT_ENTRYID
	store_entryid := messageStoreProps.PR_STORE_ENTRYID
	messageStoreProps.PR_PARENT_ENTRYID = ""
	messageStoreProps.PR_STORE_ENTRYID = ""
	item := &erp.ListItem{
		ParentEntryid: parent_entryid,
		StoreEntryid:  store_entryid,
		Prop:          messageStoreProps,
		Folders:       get_folders(messageStoreProps),
	}
	items = append(items, item)
	newState := &erp.Output{
		Success: true,
		List: &erp.ListItems{
			Items: items,
		},
	}
	d.Set(newState)
	return nil
}

// Function which is used to retrieving default folders from
// given store.
func get_folders(messageStoreProps *erp.Props) (folders []*erp.Folder) {
	var query interface{} = bson.M{
		"mdb_provider": messageStoreProps.PR_MDB_PROVIDER,
		"folder_type":  core.FOLDER_GENERIC,
	}
	if folder_records, e := mongo.Search(core.OBJECTS_TABLE, query, 0, -1); e == nil {
		for _, v := range folder_records {
			folder_props := erp.GetPropsStructs()
			b, _ := bson.Marshal(v)
			bson.Unmarshal(b, folder_props)

			entryid := folder_props.PR_ENTRYID
			parent_entryid := folder_props.PR_PARENT_ENTRYID
			store_entryid := folder_props.PR_STORE_ENTRYID
			folder_props.PR_STORE_ENTRYID = ""
			folder_props.PR_ENTRYID = ""
			folder_props.PR_PARENT_ENTRYID = ""

			folders = append(folders, &erp.Folder{
				Entryid:       entryid,
				ParentEntryid: parent_entryid,
				StoreEntryid:  store_entryid,
				Prop:          folder_props,
			})
		}
	}
	return folders
}
