package hierarchy

import (
	"github.com/ERP_v4/erp"
	"github.com/ERP_v4/schema"
)

// Provider returns a erp.ResourceProvider.
func Provider() erp.ResourceProvider {

	// The actual provider
	return &schema.Provider{
		ResourcesMap: map[string]*schema.Resource{
			"hierarchy_module": resourceHierarchyLoad(),
		},
	}
}
