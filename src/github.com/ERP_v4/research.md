Install dependencies
=============
```sh
go get -u github.com/golang/dep/cmd/dep
```

Set environment variable permanently
==============
``` sh
nano $HOME/.bashrc
```

MAPI related links
=========
 - different types providers
    - message store provider
    - transport provider
    - address book provider

 - MAPI Message store provider.
   - https://msdn.microsoft.com/en-us/library/office/cc839971.aspx
   - https://msdn.microsoft.com/en-us/library/office/cc842153.aspx

 - Create unique ID like entryid in MAPI
   - https://msdn.microsoft.com/en-us/library/office/cc842167.aspx
   - https://msdn.microsoft.com/en-us/library/office/cc842499.aspx

 - MAPI folder structure
```js
       Message store
            |
        Container
            |
        IPM_SUBTREE
            |
      Default folders
```
Request structure
=========
``` js
{
 	"provider" : "auth",
 	"resource" : "user_authentication",
 	"request_type" : "items",
 	"action" : "save",
 	"message_action" : {
 	    "send" : "true",
 	    "action_type" : "acceptTaskRequest"
 	},
 	"restriction" : {

 	},
 	"items" : [{
 		"entryid": "0001",
 		"parent_entryid": "0002",
 		"store_entryid": "0003",
 		"props" : {
 		    "entryid" : "snehal",
 		    "store_entryid" : "dangrishiya"
 		}
 	},{
 		"entryid": "0002",
 		"parent_entryid": "0002",
 		"store_entryid": "0003",
 		"props" : {
 		    "entryid" : "dhara",
 		    "store_entryid" : "dangrishiya"
 		}
 	}]
 }
```
- when item request send action type must be one of them.
    -  action :- save,delete,open
- how to get know that what action we have to execute.
    -  Save:-
        - create new record
            - if request not contains "Entryid" and contains "ParentEntryid" & "StoreEntryid" then call create method of resource
        - Update record
            - if request contains "Entryid", "ParentEntryid" & "StoreEntryid" then call update method of resource
    - Delete :- Delete record
        - if action type is delete and it contains single or multiple "Entryid" then call delete method of resource
    - Open :- Open record
        - if request contains action type to open and it contains attlist "Entryid" and "ParentEntryid"
- when list request send no need to set action type.
    - action :- ''
    - In list request we don`t have action type value so we can call list method from context.

Authentication
===
- Create new profile which contains user info
- When we creating new profile we first check for user authentication with LDAP
- Create .ERP folder in that create <profile_name> profile name is given in drop down list on login screen
- <profile_name> should contains critical information regarding user credential.


Set and get values of message (Message/Folder/Store)
====
- create an interfaces FieldReader and FieldWriter for respectively read and write fields in mongodb
- ReadFields and WriteFields function in FieldReader and FieldWriter interface.
- introduce Schema structure in Resources and provider.
- Schema should be same as Terraform Schema. Schema is optional thing but it would be nice to provider in provider
e.g.
``` js
Schema : {
    Type:        schema.TypeString,
    Required:    true,
    DefaultFunc: schema.EnvDefaultFunc("GITHUB_TOKEN", nil),
    Description: descriptions["token"],
    ErrorCode : "1"
}
```

Response
===
``` js
{
    success,
    errorMSg,
    provider,
    resource,
    action,
    items[{
        entryid,
        parent_entryid,
        store_entryid,
        props : {
            properties,
        }
    }]
}
```

login response
=====
1. when login fails
 ```{
	  Success: false,
	  errorMsg : "Error message"
    }```
2. when login succes
   ```{
   success : true,
   props [user information ]
   }```

Session check
===
``` js
provider : "auth"
resource : "session_check"
request : "item"
action : "read" ,
token : "123123123123"
```
Login Request
====
``` js
{
	"provider" : "auth" ,
	"resource" : "user_authentication" ,
	"request_type" : "item" ,
	"action" : "read" ,
	"items" : [
		{
			"username" : "snehal" ,
			"password" : "a" ,
			"token" : ""
		}
	]
}
```

Multiple items handling
===
1) Delete record
``` js
{
 	"provider" : "employee",
 	"resource" : "delete_employee",
 	"request_type" : "list",
 	"action" : "delete",
 	"items" : [{
 		"entryid": "0001",
 		"parent_entryid": "0002",
 		"store_entryid": "0003",
 	},{
 		"entryid": "0002",
 		"parent_entryid": "0002",
 		"store_entryid": "0003",
 	}]
}
```
2) Create record
- generally as of now we are not support to create multiple record with single request but in future we can provide that in that case request is looks like something
like this

```
{
 	"provider" : "employee",
 	"resource" : "create_employee",
 	"request_type" : "list",
 	"action" : "save",
 	"items" : [{
 		"entryid": "0001",
 		"parent_entryid": "0002",
 		"store_entryid": "0003",
 		"props" : {
 		    "entryid" : "snehal",
 		    "store_entryid" : "dangrishiya"
 		}
 	},{
 		"entryid": "0002",
 		"parent_entryid": "0002",
 		"store_entryid": "0003",
 		"props" : {
 		    "entryid" : "dhara",
 		    "store_entryid" : "dangrishiya"
 		}
 	}]
}
```
Hierarchy
===
Request
-----
``` js
{
	"provider" : "hierarchy" ,
	"request_type" : "list" ,
	"resource" : "hierarchy_module" ,
	"action" : "read" ,
	"list" : {
        "token" : "c761f0d3-864d-4aa1-b281-4a7248d5a6ce"
	}
}
```
Response
----
``` js
{
	"provider" : "hierarchy" ,
	"request_type" : "list" ,
	"resource" : "hierarchy_module" ,
	"action" : "read" ,
	"list": {
		"item": [{
			"store_entryid": "0000000038a1bb1005e5101aa1bb08002b2a56c2000000000000593bd44341a0a5190cc021430c000000",
			"props": {
				"entryid": "0000000038a1bb1005e5101aa1bb08002b2a56c2000000000000593bd44341a0a5190cc021430c000000",
				"store_record_key": "593bd44341a0a5190cc02144",
				"record_key": "593bd44341a0a5190cc02144",
				"display_name": "Inbox - Snehal Dangroshiya",
				"mdb_provider": "593bd44341a0a5190cc02143",
				"ipm_subtree_entryid": "000000593bd44341a0a5190cc021441593bd44541a0a5190cc02146260000",
				"ipm_wastebasket_entryid": "000000593bd44341a0a5190cc021441593bd44641a0a5190cc02147270000",
				"object_type": 1,
				"mapping_signature": "593bd44341a0a5190cc02144"
			},
			"folders": [{
				"entryid": "000000593bd44341a0a5190cc021441593bd44541a0a5190cc02146260000",
				"parent_entryid": "000000593bd44341a0a5190cc021441593bd44441a0a5190cc02145250000",
				"store_entryid": "0000000038a1bb1005e5101aa1bb08002b2a56c2000000000000593bd44341a0a5190cc021430c000000",
				"props": {
					"store_record_key": "593bd44341a0a5190cc02144",
					"record_key": "000000593bd44341a0a5190cc021441593bd44541a0a5190cc02146260000",
					"display_name": "IPM_SUBTREE",
					"mdb_provider": "593bd44341a0a5190cc02143",
					"object_type": 3,
					"folder_type": 1,
					"mapping_signature": "593bd44341a0a5190cc02144"
				}
			},{
				"entryid": "000000593bd44341a0a5190cc021441593bd44641a0a5190cc02147270000",
				"parent_entryid": "000000593bd44341a0a5190cc021441593bd44541a0a5190cc02146260000",
				"store_entryid": "0000000038a1bb1005e5101aa1bb08002b2a56c2000000000000593bd44341a0a5190cc021430c000000",
				"props": {
					"store_record_key": "593bd44341a0a5190cc02144",
					"record_key": "000000593bd44341a0a5190cc021441593bd44641a0a5190cc02147270000",
					"display_name": "Deleted Items",
					"mdb_provider": "593bd44341a0a5190cc02143",
					"object_type": 3,
					"folder_type": 1,
					"mapping_signature": "593bd44341a0a5190cc02144"
				}
			}]
		}]
	}
}
```
Build life cycle and steps verification
====
``` js
context.go
|__Graph function
    |__call ApplyGraphBuilder Build function of graph_builder_apply.go
        |__Internally called Steps function
        |__call build function of BasicGraphBuilder
```
Thoughts
----
- Implements TaskQueueBuilder which synchronize the
task which requires to follow to complete any item or list
request for example.
``` js
context
|_ execute function
    |_ which prepare the ItemTaskQueueBuilder
       which is generally used for save,update,delete single record(s)
```
e.g. To save record
--
    - first step is to authenticate requester
    - second step to extract request data and find the operation type.
    - third step to perform the operation based on requested data
    - fourth prepare output/response data
    - fifth set the output/response data to context.go
Create separate schema for LDAP
============
- To change/set the password to [LDAP admin user](https://serverfault.com/questions/556629/unknown-ldap-cn-config-admin-password).
- I don't know how the current Ubuntu packages do the initial OpenLDAP setup ' 'but both in 10.04 and 12.04 that process didn't account very well for cn=config.
- If set you should find the password in the attribute olcRootPW in
```/etc/ldap/slapd.d/cn=config/olcDatabase={0}config.ldif (its probably base64 encoded)```.
- To change the password use ldapmodify as root. Save this as an LDIF file rootpw_cnconfig.ldif:
``` sh
 dn: olcDatabase={0}config,cn=config
 changetype: modify
 replace: olcRootPW
 olcRootPW: foobar123
 Note: In order to change the root password on CentOS7 use dn: olcDatabase={2}hdb,cn=config instead of dn: olcDatabase={0}config,cn=config.
```
- Obviously set your password to something other than foobar123. Then run ldapmodify:
``` sh
$ sudo ldapmodify -Y EXTERNAL -H ldapi:/// -f rootpw_cnconfig.ldif
```
- This presumes that the LDAP server and the cn=config database can be accessed using the ldapi protocol (-H ldapi:///) and that external SASL authentication (-Y EXTERNAL) is enabled and working, which it should by default on new OpenLDAP setups in Debian and Ubuntu.
- If you look at /etc/ldap/slapd.d/cn=config/olcDatabase={0}config.ldif it should contain an attribute olcAccess:
``` apple js
 olcAccess: {0}to * by dn.exact=gidNumber=0+uidNumber=0,cn=peercred,cn=external,cn=auth manage by * break
```
 - [use of slaptest command is LDAP to generate AUTO-GENERATED FILE](http://www.zytrax.com/books/ldap/ch6/slapd-config.html#use-schemas)
 - Create ERP Schema (use apache studio as a IDE/Editor)
    - create custom Object class
    - create custom attributes.
    e.g.
        - Object class
            - erp-user
        - Attributes
            - user-name,
            - user-address,
            - user-id,
            - user-GUID

Read data using schema 
===
