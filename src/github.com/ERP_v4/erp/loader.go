package erp

import (
	"fmt"
	"io"
	"io/ioutil"
	"os"

	"github.com/hashicorp/hcl"
)

func LoadJsonRequest(request io.ReadCloser) (*Config, error) {
	raw, err := ioutil.ReadAll(request) //<--- here!

	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	obj, err := hcl.Parse(string(raw))
	if err != nil {
		return nil, fmt.Errorf(
			"Error parsing JSON document as HCL: %s", err)
	}

	// Start building the result
	hclConfig := &hclConfigurable{
		Root: obj,
	}

	return hclConfig.Config()
}
