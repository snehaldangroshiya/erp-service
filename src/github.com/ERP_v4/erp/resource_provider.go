package erp

// Interface represents different request handling functions like item,list request.
type ResourceProvider interface {
	Item(*Resource)*Output
	List(*Resource)*Output
}

type ResourceProviderFactory func() (ResourceProvider, error)
