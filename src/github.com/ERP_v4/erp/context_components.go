package erp

import "fmt"

type contextComponentFactory interface {
	ResourceProvider(uid string) (ResourceProvider, error)
	ResourceProviders() []string
}

// basicComponentFactory just calls a factory from a map directly.
type basicComponentFactory struct {
	providers map[string]ResourceProviderFactory
}

func (c *basicComponentFactory) ResourceProviders() []string {
	result := make([]string, len(c.providers))
	for k, _ := range c.providers {
		result = append(result, k)
	}

	return result
}

func (c *basicComponentFactory) ResourceProvider(uid string) (ResourceProvider, error) {
	f, ok := c.providers[uid]
	if !ok {
		return nil, fmt.Errorf("unknown provider %q", uid)
	}

	return f()
}
