package provider

import (
	"sync"
	"time"
	"log"
)

var sessionStore map[string]Client
var storageMutex sync.RWMutex

type Client struct {
	loggedIn            bool `json:"success"`
	store_id            string
	session_create_time int64
}

func init() {
	sessionStore = make(map[string]Client)
}

// Function is used to initialize the session
// for the authenticate user.
func InitSession(token, store_id string) {
	storageMutex.Lock()
	defer storageMutex.Unlock()
	var client Client
	client = Client{true, store_id, time.Now().Unix()}
	sessionStore[token] = client
}

// Function is used to check user session is still
// active.
func IsSessionActives(token string) bool {
	storageMutex.Lock()
	defer storageMutex.Unlock()
	return sessionStore[token].loggedIn
}

// Function which is used to get the session
// creation time using giving token id.
func GetSessionCreationTime(token string) int64 {
	return sessionStore[token].session_create_time
}

func GetStoreIdFromToken(token string) string {
	log.Println("session:-", sessionStore)
	storageMutex.Lock()
	defer storageMutex.Unlock()
	return sessionStore[token].store_id
}
