package provider

import (
	"log"
	"strconv"

	"github.com/ERP_v4/erp"
	"github.com/ERP_v4/erp/core"
	"github.com/ERP_v4/erp/mongo"
	"gopkg.in/mgo.v2/bson"
)

type Provider struct {
}
type StoreEntryId struct {
	Flags              string
	ProviderGUID       string
	Version            string
	Flag               string
	WrappedFlags       string
	WrappedProviderUID string
	WrappedType        string
}
type EntryId struct {
	Flags        string
	ProviderGUID string
	FolderType   string
	DatabaseGUID string
	GlobeCounter string
	Pad          string
}
func GetProvider() *Provider {
	return &Provider{}
}
func InitProvider() {
	InitMessageBoxProvider()
}
func (provider Provider) GenerateStoreEntryId(id bson.ObjectId) string {
	var storeEntryId StoreEntryId
	storeEntryId.Flags = "00000000"
	storeEntryId.ProviderGUID = "38a1bb1005e5101aa1bb08002b2a56c2"
	storeEntryId.Version = "00"
	storeEntryId.Flag = "00"
	storeEntryId.WrappedFlags = "00000000"
	storeEntryId.WrappedProviderUID = id.Hex()
	storeEntryId.WrappedType = "0c000000"
	return storeEntryId.Flags + storeEntryId.ProviderGUID + storeEntryId.Version + storeEntryId.Flag + storeEntryId.WrappedFlags + storeEntryId.WrappedProviderUID + storeEntryId.WrappedType
}

func (provider Provider) GenerateEntryId(CollectionName string, msgStore *erp.Props, props *erp.Props) string {
	var entryid EntryId
	entryid.Flags = "000000"
	entryid.ProviderGUID = msgStore.Id.Hex()
	entryid.FolderType = strconv.Itoa(core.ERP_PRIVATE_FOLDER)
	entryid.DatabaseGUID = props.Id.Hex()
	i, e := mongo.GetGlobeCounter(CollectionName)
	if e != nil {
		log.Println("ERROR: Face problem to create Entryid ")
	}
	h := strconv.FormatInt(int64(i), 16)
	entryid.GlobeCounter = h
	entryid.Pad = "0000"
	return entryid.Flags + entryid.ProviderGUID + entryid.FolderType + entryid.DatabaseGUID + entryid.GlobeCounter + entryid.Pad
}
