package provider

import (
	"log"

	"github.com/ERP_v4/erp"
	"github.com/ERP_v4/erp/core"
	"github.com/ERP_v4/erp/ldap"
	"github.com/ERP_v4/erp/mongo"

	"os"

	"gopkg.in/mgo.v2/bson"
)

type ERPMsgStoreProvider struct {
	isLDAPServer bool
}

//
func InitMessageBoxProvider() {
	msgStoreProvider := &ERPMsgStoreProvider{
		isLDAPServer: len(os.Getenv("LDAP_SERVER")) != 0,
	}
	users := msgStoreProvider.getAllUsersFromBackEnd()
	for _, v := range users {
		row, err := mongo.GetUserByDisplayName(core.STORES_TABLE, v.(string), 0, -1)
		if err != nil {
			log.Println("ERROR: while retriving record from MongoDB")
		}

		if row == nil {
			CreateMessageStore(v.(string))
		}
	}
}

func (m *ERPMsgStoreProvider) getAllUsersFromBackEnd() (users []interface{}) {
	var err error
	if m.isLDAPServer {
		if users, err = ldap.GetAllUsers(); err != nil {
			log.Println("ERROR : while retriving user info from LDAP server")
		}
	} else {
		if users, err = ldap.GetAllUsers(); err != nil {
			log.Println("ERROR : while retriving user info from LDAP server")
		}
	}

	return users
}

func CreateMessageStore(userName string) {
	props := erp.GetPropsStructs()
	props.PR_DISPLAY_NAME = userName
	props.Id = bson.NewObjectId()
	var err error
	if err = mongo.CreateDocument(core.STORES_TABLE, props); err != nil {
		log.Println("Could not save the entry to MongoDB:", err)
	}
	props.PR_STORE_ENTRYID = GetProvider().GenerateStoreEntryId(props.Id)
	props.PR_ENTRYID = props.PR_STORE_ENTRYID
	props.PR_DISPLAY_NAME = "Inbox - " + userName
	props.PR_MDB_PROVIDER = props.Id.Hex() //storeObjId.Hex()
	props.Id = bson.NewObjectId()
	props.PR_STORE_RECORD_KEY = props.Id.Hex()
	props.PR_RECORD_KEY = props.PR_STORE_RECORD_KEY
	props.PR_OBJECT_TYPE = core.MAPI_STORE
	props.PR_MAPPING_SIGNATURE = props.PR_STORE_RECORD_KEY
	err = mongo.CreateDocument(core.OBJECTS_TABLE, props)
	if err != nil {
		log.Println("ERROR : Could not save the entry to MongoDB:", err)
	}
	if err = CreateRootContainer(props); err != nil {
		log.Println("ERROR: Root container is not created properly")
	}
}

func CreateRootContainer(msgStore *erp.Props) error {
	props := erp.GetPropsStructs()
	props.PR_FOLDER_TYPE = core.FOLDER_ROOT
	props.PR_SUBFOLDERS = true
	entryid, err := CreateFolder(props, msgStore, msgStore)
	if err != nil {
		log.Println("Error : IPM Subtree is not created properly", err)
	}
	props.PR_PARENT_ENTRYID = entryid
	// we need to remove id property from props
	// as we use that props to update record.
	id := props.Id
	props.Id = ""
	if err := mongo.UpdateDocument(core.OBJECTS_TABLE, id, props); err != nil {
		log.Println("ERROR : Could not save the parent entry of root container to MongoDB:", err)
	}
	CreateDefaultFolders(props)
	return err
}

// Function which is used to retrieve Default store
func DefaultMessageStore(user string) *erp.Props {
	// Get the user from  store table
	// based on store table record get
	type user_store struct {
		Id bson.ObjectId `bson:"_id,omitempty" json:"-"`
	}
	userStoreProps := &user_store{}
	var query interface{} = bson.M{"display_name": user}
	if msgStoreProps, e := mongo.Search(core.STORES_TABLE, query, 0, 1); e == nil {
		for _, v := range msgStoreProps {
			b, _ := bson.Marshal(v)
			bson.Unmarshal(b, userStoreProps)
		}
	}

	props := erp.GetPropsStructs()
	var queryToFindDefaultStore interface{} = bson.M{
		"mdb_provider": userStoreProps.Id.Hex(),
		"object_type":  core.MAPI_STORE,
	}
	if messageStoreProps, e := mongo.Search(core.OBJECTS_TABLE, queryToFindDefaultStore, 0, 1); e == nil {
		for _, v := range messageStoreProps {
			b, _ := bson.Marshal(v)
			bson.Unmarshal(b, props)
		}
	}
	return props
}

// Function which used get root container of login user
func getRootContainer() *erp.Props {
	props := erp.GetPropsStructs()
	return props
}

func CreateDefaultFolders(parent *erp.Props) {
	if err := CreateIPMSubTree(parent); err != nil {
		log.Println("Error : IMP_SUBTREE is not created properly", err)
	}
}

// Function which create the folder record in backend.
func CreateFolder(props *erp.Props, parent *erp.Props, store *erp.Props) (string, error) {
	props.Id = bson.NewObjectId()
	props.PR_OBJECT_TYPE = core.MAPI_FOLDER
	props.PR_STORE_ENTRYID = parent.PR_STORE_ENTRYID
	props.PR_MDB_PROVIDER = parent.PR_MDB_PROVIDER
	props.PR_MAPPING_SIGNATURE = parent.PR_MAPPING_SIGNATURE
	props.PR_STORE_RECORD_KEY = parent.PR_MAPPING_SIGNATURE
	// generate Entryid for default folders
	props.PR_ENTRYID = GetProvider().GenerateEntryId(core.OBJECTS_TABLE, store, props)
	props.PR_RECORD_KEY = props.PR_ENTRYID
	// Create document for the default folder in Object table
	err := mongo.CreateDocument(core.OBJECTS_TABLE, props)
	if err != nil {
		log.Println("ERROR : Could not save the entry to MongoDB:", err)
	}
	return props.PR_ENTRYID, err
}

func GetMsgStoreObj(folderProps *erp.Props) *erp.Props {
	props := erp.GetPropsStructs()
	var query interface{} = bson.M{"store_entryid": folderProps.PR_STORE_ENTRYID, "object_type": core.MAPI_STORE}
	if msgStoreProps, e := mongo.Search(core.OBJECTS_TABLE, query, 0, 1); e == nil {
		for _, v := range msgStoreProps {
			b, _ := bson.Marshal(v)
			bson.Unmarshal(b, props)
		}
	}
	return props
}

func CreateIPMSubTree(parent *erp.Props) error {
	props := erp.GetPropsStructs()
	props.PR_PARENT_ENTRYID = parent.PR_ENTRYID
	props.PR_FOLDER_TYPE = core.FOLDER_GENERIC
	props.PR_DISPLAY_NAME = "IPM_SUBTREE"
	msgStore := GetMsgStoreObj(parent)
	entryid, err := CreateFolder(props, parent, msgStore)
	if err != nil {
		log.Println("Error : IPM Subtree is not created properly", err)
	}
	msgStore.PR_IPM_SUBTREE_ENTRYID = entryid
	id := msgStore.Id
	msgStore.Id = ""

	// Update message store document in object store.
	if err := mongo.UpdateDocument(core.OBJECTS_TABLE, id, msgStore); err != nil {
		log.Println("ERROR : Could not save the entry to MongoDB:", err)
	}
	if err := CreateDeletedItemsFolder(props); err != nil {
		log.Println("Error : Deleted items folder is not created properly", err)
	}
	return err
}

func CreateDeletedItemsFolder(parent *erp.Props) error {
	props := erp.GetPropsStructs()
	props.PR_PARENT_ENTRYID = parent.PR_ENTRYID
	props.PR_FOLDER_TYPE = core.FOLDER_GENERIC
	props.PR_DISPLAY_NAME = "Deleted Items"
	msgStore := GetMsgStoreObj(parent)
	entryid, err := CreateFolder(props, parent, msgStore)
	if err != nil {
		log.Println("Error : IPM Subtree is not created properly", err)
	}
	msgStore.PR_IPM_WASTEBASKET_ENTRYID = entryid
	id := msgStore.Id
	msgStore.Id = ""

	// Update message store document in object store.
	if err := mongo.UpdateDocument(core.OBJECTS_TABLE, id, msgStore); err != nil {
		log.Println("ERROR : Could not save the entry to MongoDB:", err)
	}
	return err
}
