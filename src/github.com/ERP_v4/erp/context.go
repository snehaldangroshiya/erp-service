package erp

import (
	"log"
)

// ContextOpts are the user-configurable options to create a context with
// NewContext.
type ContextOpts struct {
	Providers map[string]ResourceProviderFactory
	Resource
	ProviderConfig
}

type Context struct {
	components   contextComponentFactory
	resource     *Resource
	providerName string
	output       *Output
}

// Function is create the context object from given request information.
func NewContext(opts *ContextOpts) (*Context, error) {
	return &Context{
		components: &basicComponentFactory{
			providers: opts.Providers,
		},
		resource:     &opts.Resource,
		providerName: opts.ProviderConfig.Name,
	}, nil
}

func (c *Context) item() error {
	provider, err := c.getProvider()
	if err != nil {
		log.Printf("[Error] Provider not found")
	}
	log.Println("[LOG 4]: item function of provider is called")
	c.output = provider.Item(c.resource)
	return nil
}

func (c *Context) list() error {
	provider, err := c.getProvider()
	if err != nil {
		log.Printf("[Error] Provider not found")
	}
	c.output = provider.List(c.resource)
	return nil
}

func (c *Context) Execute() error {
	switch c.resource.RequestType {
	case ItemRequest:
		c.item()
		break
	case ListRequest:
		c.list()
		break
	}
	return nil
}

func (c *Context) getProvider() (ResourceProvider, error) {
	return c.components.ResourceProvider(c.providerName)
}

func (c *Context) Output() *Output {
	c.output.Provider = c.resource.Provider
	c.output.Action = c.resource.Action
	c.output.ResourceName = c.resource.Name
	return c.output
}
