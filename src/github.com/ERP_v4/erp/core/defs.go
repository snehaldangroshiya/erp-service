package core

const (
	MAPI_STORE               = 0x00000001 /* Message Store */
	MAPI_ADDRBOOK            = 0x00000002 /* Address Book */
	MAPI_FOLDER              = 0x00000003 /* Folder */
	MAPI_ABCONT              = 0x00000004 /* Address Book Container */
	MAPI_MESSAGE             = 0x00000005 /* Message */
	MAPI_MAILUSER            = 0x00000006 /* Individual Recipient */
	MAPI_ATTACH              = 0x00000007 /* Attachment */
	MAPI_DISTLIST            = 0x00000008 /* Distribution List*/
	TABLE_SORT_ASCEND        = 0x00000000
	TABLE_SORT_DESCEND       = 0x00000001
	TABLE_SORT_COMBINE       = 0x00000002
	ERP_SERVICE_GUID         = "87d7e997a92144bb8c535df3b4bfe5f3" // default store
	ERP_STORE_PUBLIC_GUID    = "2fa9730538574463b1756806b3782d35"
	ERP_STORE_DELEGATE_GUID  = "2fa9730538574463b1756806b3782d36"
	ERP_STORE_ARCHIVER_GUID  = "2fa9730538574463b1756806b3782d37"
	STORES_TABLE             = "stores"
	OBJECTS_TABLE            = "objects"
	SESSIONS_TABLE           = "sessions"
	DB_USER_TABLE            = "users"
	ERP_PRIVATE_FOLDER       = 0X001
	ERP_PUBLIC_FOLDER        = 0X003
	ERP_MAPPEDPUBLIC_FOLDER  = 0X005
	ERP_PRIVATE_MESSAGE      = 0X007
	ERP_PUBLIC_MESSAGE       = 0X009
	ERP_MAPPEDPUBLIC_MESSAGE = 0X00B
	FOLDER_ROOT              = 0x00000000
	FOLDER_GENERIC           = 0x00000001
	FOLDER_SEARCH            = 0x00000002
)
