package erp

type Config struct {
	ProviderConfigs *ProviderConfig
	Resources       *Resource
	Outputs         []*Output
}

// ProviderConfig is the configuration for a resource provider.
type ProviderConfig struct {
	Name string
}

// A resource represents a single ERP resource in the configuration.
// A ERP resource is something that supports some or all of the
// usual "create, read, update, delete" operations
type Resource struct {
	Name        string
	Provider    string
	Action      string
	RequestType string
	RawConfig   *RawConfig
}

type Item struct {
	Entryid       string `json:"entryid,omitempty"`
	ParentEntryid string `json:"parent_entryid,omitempty"`
	StoreEntryid  string `json:"store_entryid, omitempty"`
	Prop          Props  `json:"props, omitempty"`
}

type ListItem struct {
	Entryid       string    `json:"entryid,omitempty"`
	ParentEntryid string    `json:"parent_entryid,omitempty"`
	StoreEntryid  string    `json:"store_entryid, omitempty"`
	Prop          *Props    `json:"props, omitempty"`
	Folders       []*Folder `json:"folders, omitempty"`
}

type ListItems struct {
	Items []*ListItem `json:"item,omitempty"`
}

type Folder struct {
	Entryid       string `json:"entryid,omitempty"`
	ParentEntryid string `json:"parent_entryid,omitempty"`
	StoreEntryid  string `json:"store_entryid, omitempty"`
	Prop          *Props `json:"props, omitempty"`
}

type Output struct {
	Success      bool       `json:"success"`
	ErrorMsg     string     `json:"error,omitempty"`
	Token        string     `json:"token,omitempty"`
	Provider     string     `json:"provider,omitempty"`
	ResourceName string     `json:"resource,omitempty"`
	Action       string     `json:"action,omitempty"`
	Items        []*Item    `json:"items,omitempty"`
	List         *ListItems `json:"list,omitempty"`
}
