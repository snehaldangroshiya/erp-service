package ldap

import (
	"fmt"
	"log"

	"os"

	"gopkg.in/ldap.v2"
)

type Ldap_config struct {
	UserName   string
	Password   string
	LDAPServer string
}

func GetLDAP() *Ldap_config {
	return &Ldap_config{
		LDAPServer: os.Getenv("LDAP_SERVER"),
	}
}

func (lconfig *Ldap_config) getLDAPServer() string {
	var ldapServer string
	if len(lconfig.LDAPServer) == 0 {
		ldapServer = os.Getenv("LDAP_SERVER")
	} else {
		ldapServer = lconfig.LDAPServer
	}
	return ldapServer
}

func (lconfig *Ldap_config) getSession() (l *ldap.Conn, err error) {
	l, err = ldap.Dial("tcp", fmt.Sprintf("%s:%d", lconfig.getLDAPServer(), 389))
	if err != nil {
		log.Fatal(err)
	}
	return l, nil
}

func (lconfig *Ldap_config) Authentication() (bool, string) {
	l, _ := lconfig.getSession()
	defer l.Close()

	// Search for the given username
	searchRequest := ldap.NewSearchRequest(
		LDAPUser,
		ldap.ScopeWholeSubtree, ldap.NeverDerefAliases, 0, 0, false,
		fmt.Sprintf("(&(uid=%s))", lconfig.UserName),
		[]string{"dn", "cn"},
		nil,
	)

	sr, err := l.Search(searchRequest)
	if err != nil {
		return false, ""
	}

	if len(sr.Entries) != 1 {
		return false, ""
	}

	userdn := sr.Entries[0].DN

	// Bind as the user to verify their password
	err = l.Bind(userdn, lconfig.Password)
	if err != nil {
		return false, ""
	}
	return true, sr.Entries[0].GetAttributeValue("cn")
}

// Function retrieve all users from LDAP server.
func GetAllUsers() (users []interface{}, error error) {
	l, err := ldap.Dial("tcp", fmt.Sprintf("%s:%d", LDAPAddress, 389))
	if err != nil {
		log.Fatal("Error:- ", err)
	}
	defer l.Close()

	searchRequest := ldap.NewSearchRequest(
		LDAPUser, // The base dn to search
		ldap.ScopeWholeSubtree, ldap.NeverDerefAliases, 0, 0, false,
		"(&(objectClass="+Object_class+"))",                // The filter to apply
		[]string{"dn", "cn", "sn", "mail", "userpassword"}, // A list attributes to retrieve
		nil,
	)

	sr, err := l.Search(searchRequest)
	if err != nil {
		error = err
		log.Fatal(err)
	}

	for _, entry := range sr.Entries {
		users = append(users, entry.GetAttributeValue("cn"))
	}
	return
}
