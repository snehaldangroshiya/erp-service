package ldap

const (
	LDAPAddress  string = "10.0.0.13"
	LDAPUser     string = "dc=kopano,dc=local"
	Object_class string = "kopano-user"
)
