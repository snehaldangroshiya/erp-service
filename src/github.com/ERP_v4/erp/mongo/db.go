package mongo

import (
	"crypto/tls"
	"log"
	"net"

	"github.com/ERP_v4/erp/core"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

var (
	mgoSession   *mgo.Session
	databaseName = "erp"
)

//Function used to get the MongoDB session object
func getSession() *mgo.Session {
	if mgoSession == nil {
		DialInfo, _ := mgo.ParseURL("mongodb://snehal:VXmWJuqjM8CdtzOa@erp-shard-00-00-kod8k.mongodb.net:27017,erp-shard-00-01-kod8k.mongodb.net:27017,erp-shard-00-02-kod8k.mongodb.net:27017/erp?replicaSet=ERP-shard-0&authSource=admin")
		tlsConfig := &tls.Config{}
		DialInfo.DialServer = func(addr *mgo.ServerAddr) (net.Conn, error) {
			conn, err := tls.Dial("tcp", addr.String(), tlsConfig)
			return conn, err
		}

		session, err := mgo.DialWithInfo(DialInfo)
		if err != nil {
			log.Println(err)
		}
		mgoSession = session
	}

	return mgoSession.Clone()
}

//
//
func withCollection(collection string, s func(*mgo.Collection) error) error {
	session := getSession()
	defer session.Close()
	c := session.DB(databaseName).C(collection)
	return s(c)
}

//
//
func Search(collectionName string, q interface{}, skip int, limit int) (searchResults []bson.M, e error) {
	query := func(c *mgo.Collection) error {
		fn := c.Find(q).Skip(skip).Limit(limit).All(&searchResults)
		if limit < 0 {
			fn = c.Find(q).Skip(skip).All(&searchResults)
		}
		return fn
	}
	search := func() error {
		return withCollection(collectionName, query)
	}
	err := search()
	if err != nil {
		e = err
	}
	return
}

//
func GetUserByDisplayName(collectionName string, name string, skip int, limit int) (searchResults []bson.M, err error) {
	searchResults, err = Search(collectionName, bson.M{"display_name": name}, skip, limit)
	return
}

// Function used to retrieve the document from collection by given name.
func GetIDByDisplayName(collectionName string, name string, skip int, limit int) (_id bson.ObjectId, err error) {
	var searchResults []bson.M
	searchResults, err = Search(collectionName, bson.M{"display_name": name}, skip, limit)
	Obj, _ := searchResults[0]["_id"].(bson.ObjectId)
	_id = Obj
	return
}

// Function was create the new document record in
// given collection.
func CreateDocument(collectionName string, props interface{}) error {
	query := func(c *mgo.Collection) error {
		fn := c.Insert(props)
		return fn
	}
	insert := func() error {
		return withCollection(collectionName, query)
	}
	return insert()
}

// update given collection document with given properties
func UpdateDocument(collectionName string, id bson.ObjectId, props interface{}) error {
	query := func(c *mgo.Collection) error {
		fn := c.UpdateId(id, bson.M{"$set": props})
		return fn
	}
	update := func() error {
		return withCollection(collectionName, query)
	}
	return update()
}

// Function used to get the total number of record on Object
// collection in mongoDB
func GetGlobeCounter(collectionName string) (int, error) {
	session := getSession()
	defer session.Close()
	return session.DB(databaseName).C(collectionName).Count()
}

// Helper function which used to check
// given collection is exist or not.
func IsCollectionExist(collectionName string) (isExist bool) {
	query := func(c *mgo.Collection) error {
		isExist = c.Name == collectionName
		return nil
	}

	_ = withCollection(collectionName, query)
	return
}

// TODO: Currently working on this is part
type UserCredential struct {
	UserName string
	Password string
}

func GetUserDetails(uname string, pass string) *UserCredential {
	return &UserCredential{
		UserName: uname,
		Password: pass,
	}
}

func (u *UserCredential) Authentication() (isAuth bool, displayName string) {
	var searchResults interface{}
	query := func(c *mgo.Collection) error {
		var q interface{} = bson.M{"user_name": u.UserName, "password": u.Password}
		fn := c.Find(q).Distinct("display_name", &searchResults)
		return fn
	}
	if error := withCollection(core.DB_USER_TABLE, query); error != nil {
		log.Println("[Erro]: while authenticating users.")
	}

	isAuth = len(searchResults.([]interface{})) == 1
	if isAuth {
		for _, v := range searchResults.([]interface{}) {
			displayName = v.(string)
		}
	}
	return
}

func (u *UserCredential) IsExist() bool {
	var searchResults interface{}
	query := func(c *mgo.Collection) error {
		var q interface{} = bson.M{"user_name": u.UserName}
		fn := c.Find(q).Distinct("display_name", &searchResults)
		return fn
	}
	if error := withCollection(core.DB_USER_TABLE, query); error != nil {
		log.Println("[Erro]: while authenticating users.")
	}
	return len(searchResults.([]interface{})) == 1
}
