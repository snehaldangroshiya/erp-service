package erp

import (
	"sync"
)

type RawConfig struct {
	Key string
	Raw []map[string]interface{}

	config []map[string]interface{}
	lock   sync.Mutex
}

// NewRawConfig creates a new RawConfig structure and populates the
// publicly readable struct fields.
func NewRawConfig(raw []map[string]interface{}) (*RawConfig, error) {
	result := &RawConfig{Raw: raw}
	if err := result.init(); err != nil {
		return nil, err
	}

	return result, nil
}

func (r *RawConfig) init() error {
	r.lock.Lock()
	defer r.lock.Unlock()

	r.config = r.Raw
	return nil
}

func (r *RawConfig) Config() []map[string]interface{} {
	r.lock.Lock()
	defer r.lock.Unlock()
	return r.config
}

// Function which is used to get the schema properties from
// request data.
func (r *RawConfig) Get(key string) interface{} {
	r.lock.Lock()
	defer r.lock.Unlock()
	var items interface{}
	for _, raw := range r.Raw {
		if raw[key] != nil {
			items = raw[key]
		}
	}
	return items
}
