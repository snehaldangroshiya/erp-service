package erp

import "gopkg.in/mgo.v2/bson"

// Fixme :
type Props struct {
	Id                            bson.ObjectId `bson:"_id,omitempty" json:"-"`
	PR_ENTRYID                    string        `json:"entryid,omitempty" mapstructure:"entryid" bson:"entryid,omitempty"`
	PR_PARENT_ENTRYID             string        `json:"parent_entryid,omitempty" mapstructure:"parent_entryid" bson:"parent_entryid,omitempty"`
	PR_STORE_ENTRYID              string        `json:"store_entryid,omitempty" mapstructure:"store_entryid" bson:"store_entryid,omitempty"`
	PR_STORE_RECORD_KEY           string        `json:"store_record_key,omitempty" mapstructure:"store_record_key" bson:"store_record_key,omitempty"`
	PR_RECORD_KEY                 string        `json:"record_key,omitempty" mapstructure:"record_key" bson:"record_key,omitempty"`
	PR_DISPLAY_NAME               string        `json:"display_name,omitempty" mapstructure:"display_name" bson:"display_name,omitempty"`
	PR_DEFAULT_STORE              bool          `json:"default_store,omitempty" mapstructure:"default_store" bson:"default_store,omitempty"`
	PR_STORE_SUPPORT_MASK         int           `json:"store_support_mask,omitempty" mapstructure:"store_support_mask" bson:"store_support_mask,omitempty"`
	PR_STORE_STATE                int           `json:"store_state,omitempty" mapstructure:"store_state" bson:"store_state,omitempty"`
	PR_IPM_SUBTREE_SEARCH_KEY     string        `json:"ipm_subtree_search_key,omitempty" mapstructure:"ipm_subtree_search_key" bson:"ipm_subtree_search_key ,omitempty"`
	PR_IPM_OUTBOX_SEARCH_KEY      string        `json:"ipm_outbox_search_key,omitempty" mapstructure:"ipm_outbox_search_key" bson:"ipm_outbox_search_key,omitempty"`
	PR_IPM_WASTEBASKET_SEARCH_KEY string        `json:"ipm_wastebasket_search_key,omitempty" mapstructure:"ipm_wastebasket_search_key" bson:"ipm_wastebasket_search_key,omitempty"`
	PR_IPM_SENTMAIL_SEARCH_KEY    string        `json:"ipm_sentmail_search_key,omitempty" mapstructure:"ipm_sentmail_search_key" bson:"ipm_sentmail_search_key,omitempty"`
	PR_MDB_PROVIDER               string        `json:"mdb_provider,omitempty" mapstructure:"mdb_provider" bson:"mdb_provider,omitempty"`
	PR_RECEIVE_FOLDER_SETTINGS    string        `json:"receive_folder_settings,omitempty" mapstructure:"receive_folder_settings" bson:"receive_folder_settings,omitempty"`
	PR_VALID_FOLDER_MASK          string        `json:"valid_folder_mask ,omitempty" mapstructure:"valid_folder_mask" bson:"valid_folder_mask,omitempty"`
	PR_IPM_SUBTREE_ENTRYID        string        `json:"ipm_subtree_entryid,omitempty" mapstructure:"ipm_subtree_entryid" bson:"ipm_subtree_entryid,omitempty"`
	PR_IPM_OUTBOX_ENTRYID         string        `json:"ipm_outbox_entryid,omitempty" mapstructure:"ipm_outbox_entryid" bson:"ipm_outbox_entryid,omitempty"`
	PR_IPM_WASTEBASKET_ENTRYID    string        `json:"ipm_wastebasket_entryid,omitempty" mapstructure:"ipm_wastebasket_entryid" bson:"ipm_wastebasket_entryid,omitempty"`
	PR_IPM_SENTMAIL_ENTRYID       string        `json:"ipm_sentmail_entryid,omitempty" mapstructure:"ipm_sentmail_entryid" bson:"ipm_sentmail_entryid,omitempty"`
	PR_VIEWS_ENTRYID              string        `json:"views_entryid,omitempty" mapstructure:"views_entryid" bson:"views_entryid,omitempty"`
	PR_COMMON_VIEWS_ENTRYID       string        `json:"common_views_entryid,omitempty" mapstructure:"common_views_entryid" bson:"common_views_entryid,omitempty"`
	PR_FINDER_ENTRYID             string        `json:"finder_entryid,omitempty" mapstructure:"finder_entryid" bson:"finder_entryid,omitempty"`
	PR_IPM_FAVORITES_ENTRYID      string        `json:"ipm_favorites_entryid,omitempty" mapstructure:"ipm_favorites_entryid" bson:"ipm_favorites_entryid,omitempty"`
	PR_IPM_PUBLIC_FOLDERS_ENTRYID string        `json:"ipm_public_folders_entryid,omitempty" mapstructure:"ipm_public_folders_entryid" bson:"ipm_public_folders_entryid,omitempty"`
	PR_IPM_APPOINTMENT_ENTRYID    string        `json:"ipm_appointment_entryid,omitempty" mapstructure:"ipm_appointment_entryid" bson:"ipm_appointment_entryid,omitempty"`
	PR_IPM_CONTACT_ENTRYID        string        `json:"ipm_contact_entryid,omitempty" mapstructure:"ipm_contact_entryid" bson:"ipm_contact_entryid,omitempty"`
	PR_IPM_JOURNAL_ENTRYID        string        `json:"ipm_journal_entryid,omitempty" mapstructure:"ipm_journal_entryid" bson:"ipm_journal_entryid,omitempty"`
	PR_IPM_NOTE_ENTRYID           string        `json:"ipm_note_entryid,omitempty" mapstructure:"ipm_note_entryid" bson:"ipm_note_entryid,omitempty"`
	PR_IPM_TASK_ENTRYID           string        `json:"ipm_task_entryid,omitempty" mapstructure:"ipm_task_entryid" bson:"ipm_task_entryid,omitempty"`
	PR_IPM_DRAFTS_ENTRYID         string        `json:"ipm_drafts_entryid,omitempty" mapstructure:"ipm_drafts_entryid" bson:"ipm_drafts_entryid,omitempty"`
	PR_EC_ERP_SETTINGS_JSON       string        `json:"ec_erp_settings_json,omitempty" mapstructure:"ec_erp_settings_json" bson:"ec_erp_settings_json,omitempty"`
	PR_OBJECT_TYPE                int           `json:"object_type,omitempty" mapstructure:"object_type" bson:"object_type,omitempty"`
	PR_FOLDER_TYPE                int            `json:"folder_type,omitempty" mapstructure:"folder_type" bson:"folder_type,omitempty"`
	PR_MAPPING_SIGNATURE          string        `json:"mapping_signature,omitempty" mapstructure:"mapping_signature" bson:"mapping_signature,omitempty"`
	PR_SUBFOLDERS                 bool          `json:"subfolders,omitempty" mapstructure:"subfolders" bson:"subfolders,omitempty"`
	PR_ACCESS                     int           `json:"access,omitempty" mapstructure:"access" bson:"access,omitempty"`
	PR_SUBJECT                    string        `json:"subject,omitempty" mapstructure:"subject" bson:"subject,omitempty"`
	PR_ICON_INDEX                 int           `json:"icon_index,omitempty" mapstructure:"icon_index" bson:"icon_index,omitempty"`
	PR_MESSAGE_CLASS              string        `json:"message_class,omitempty" mapstructure:"message_class" bson:"message_class,omitempty"`
	PR_MESSAGE_FLAGS              string        `json:"message_flags,omitempty" mapstructure:"message_flags" bson:"message_flags,omitempty"`
	PR_MESSAGE_SIZE               int           `json:"message_size,omitempty" mapstructure:"message_size" bson:"message_size,omitempty"`
	PR_MIDDLE_NAME                string        `json:"middle_name,omitempty" mapstructure:"middle_name" bson:"middle_name,omitempty"`
	PR_SURNAME                    string        `json:"surname,omitempty" mapstructure:"surname" bson:"surname,omitempty"`
	PR_HOME_TELEPHONE_NUMBER      int           `json:"home_telephone_number,omitempty" mapstructure:"home_telephone_number" bson:"home_telephone_number,omitempty"`
	PR_CELLULAR_TELEPHONE_NUMBER  int           `json:"cellular_telephone_number,omitempty" mapstructure:"cellular_telephone_number" bson:"cellular_telephone_number,omitempty"`
	PR_BUSINESS_TELEPHONE_NUMBER  int           `json:"business_telephone_number,omitempty" mapstructure:"business_telephone_number" bson:"business_telephone_number,omitempty"`
	PR_BUSINESS_FAX_NUMBER        int           `json:"business_fax_number,omitempty" mapstructure:"business_fax_number" bson:"business_fax_number,omitempty"`
	PR_COMPANY_NAME               string        `json:"company_name,omitempty" mapstructure:"company_name" bson:"company_name,omitempty"`
	PR_DEPARTMENT_NAME            string        `json:"department_name,omitempty" mapstructure:"department_name" bson:"department_name,omitempty"`
	PR_OFFICE_LOCATION            string        `json:"office_location,omitempty" mapstructure:"office_location" bson:"office_location,omitempty"`
	PR_PROFESSION                 string        `json:"profession,omitempty" mapstructure:"profession" bson:"profession,omitempty"`
	PR_MANAGER_NAME               string        `json:"manager_name,omitempty" mapstructure:"manager_name" bson:"manager_name,omitempty"`
	PR_ASSISTANT                  string        `json:"assistant,omitempty" mapstructure:"assistant" bson:"assistant,omitempty"`
	PR_NICKNAME                   string        `json:"nickname,omitempty" mapstructure:"nickname" bson:"nickname,omitempty"`
	PR_DISPLAY_NAME_PREFIX        string        `json:"display_name_prefix,omitempty" mapstructure:"display_name_prefix" bson:"display_name_prefix,omitempty"`
	PR_SPOUSE_NAME                string        `json:"spouse_name,omitempty" mapstructure:"spouse_name" bson:"spouse_name,omitempty"`
	PR_GENERATION                 string        `json:"generation,omitempty" mapstructure:"generation" bson:"generation,omitempty"`
	PR_BIRTHDAY                   string        `json:"birthday,omitempty" mapstructure:"birthday" bson:"birthday,omitempty"`
	PR_WEDDING_ANNIVERSARY        string        `json:"wedding_anniversary,omitempty" mapstructure:"wedding_anniversary" bson:"wedding_anniversary,omitempty"`
	PR_SENSITIVITY                string        `json:"sensitivity,omitempty" mapstructure:"sensitivity" bson:"sensitivity,omitempty"`
	PR_HASATTACH                  bool          `json:"hasattach,omitempty" mapstructure:"hasattach" bson:"hasattach,omitempty"`
	PR_COUNTRY                    string        `json:"country,omitempty" mapstructure:"country" bson:"country,omitempty"`
	PR_LOCALITY                   string        `json:"locality,omitempty" mapstructure:"locality" bson:"locality,omitempty" `
	PR_POSTAL_ADDRESS             string        `json:"postal_address,omitempty" mapstructure:"postal_address" bson:"postal_address,omitempty"`
	PR_POSTAL_CODE                int           `json:"postal_code,omitempty" mapstructure:"postal_code" bson:"postal_code,omitempty"`
	PR_STATE_OR_PROVINCE          string        `json:"state_or_province,omitempty" mapstructure:"state_or_province" bson:"state_or_province,omitempty"`
	PR_STREET_ADDRESS             string        `json:"street_address,omitempty" mapstructure:"street_address" bson:"street_address,omitempty"`
	PR_SESSION_ID                 string        `json:"session_id,omitempty" mapstructure:"session_id" bson:"session_id,omitempty"`
}

func GetPropsStructs() *Props {
	return &Props{}
}
