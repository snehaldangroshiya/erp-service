package erp

import (
	"fmt"
	"github.com/hashicorp/hcl"
	"github.com/hashicorp/hcl/hcl/ast"
	"github.com/mitchellh/mapstructure"
)

// hclConfigurable is an implementation of configurable that knows
// how to turn HCL configuration into a *Config object.
type hclConfigurable struct {
	Root *ast.File
}

func (t *hclConfigurable) Config() (*Config, error) {
	// Start building up the actual configuration.
	config := new(Config)

	// Top-level item should be the object list
	list, ok := t.Root.Node.(*ast.ObjectList)
	if !ok {
		return nil, fmt.Errorf("error parsing: file doesn't contain a root object")
	}

	if provider := list.Filter("provider"); len(provider.Items) > 0 {
		var err error
		config.ProviderConfigs, err = loadProviderJson(provider)
		if err != nil {
			return nil, err
		}
	}

	{
		config.Resources, _ = loadResourceJson(list)
	}
	return config, nil
}

func loadProviderJson(list *ast.ObjectList) (*ProviderConfig, error) {
	var n string
	for _, item := range list.Items {
		if err := hcl.DecodeObject(&n, item.Val); err != nil {
			return nil, err
		}
	}

	return &ProviderConfig{
		Name: n,
	}, nil
}

func loadItemsJson(list *ast.ObjectList) ([]*Item, error) {
	// Go through each object and turn it into an actual result.
	result := make([]*Item, 0, len(list.Items))
	var entryid string
	var parent_entryid string
	var store_entryid string

	var props Props
	for _, item := range list.Items {
		var listVal *ast.ObjectList
		if ot, ok := item.Val.(*ast.ObjectType); ok {
			listVal = ot.List
		} else {
			return nil, nil
		}
		if items := listVal.Filter("entryid"); len(items.Items) > 0 {
			if err := hcl.DecodeObject(&entryid, items.Items[0].Val); err != nil {
				return nil, err
			}
		}
		if items := listVal.Filter("parent_entryid"); len(items.Items) > 0 {
			if err := hcl.DecodeObject(&parent_entryid, items.Items[0].Val); err != nil {
				return nil, err
			}
		}

		if items := listVal.Filter("store_entryid"); len(items.Items) > 0 {
			if err := hcl.DecodeObject(&store_entryid, items.Items[0].Val); err != nil {
				return nil, err
			}
		}

		if items := listVal.Filter("props"); len(items.Items) > 0 {
			var propsVal *ast.ObjectList
			if ot, ok := items.Items[0].Val.(*ast.ObjectType); ok {
				propsVal = ot.List
			} else {
				return nil, nil
			}

			var prop map[string]interface{}
			if err := hcl.DecodeObject(&prop, propsVal.Children()); err != nil {
				return nil, err
			}

			if err := mapstructure.Decode(prop, &props); err != nil {
				return nil, err
			}
		}
		result = append(result, &Item{
			Entryid:       entryid,
			ParentEntryid: parent_entryid,
			StoreEntryid:  store_entryid,
			Prop:          props,
		})
	}
	return result, nil
}

func loadResourceJson(list *ast.ObjectList) (*Resource, error) {
	list = list.Children()
	if len(list.Items) == 0 {
		return nil, nil
	}

	var providerName string
	var resourceName string
	var requestType string
	var action string
	var config []map[string]interface{}
	var items_list_Data map[string]interface{}

	// Now go over all the types and their children in order to get
	// all of the actual resources.
	for _, item := range list.Items {
		var key = item.Keys[0].Token.Value().(string)
		switch key {
		case "provider":
			if err := hcl.DecodeObject(&providerName, item.Val); err != nil {
				return nil, err
			}
			break
		case "resource":
			if err := hcl.DecodeObject(&resourceName, item.Val); err != nil {
				return nil, err
			}
			break
		case "action":
			if err := hcl.DecodeObject(&action, item.Val); err != nil {
				return nil, err
			}
			break
		case "request_type":
			if err := hcl.DecodeObject(&requestType, item.Val); err != nil {
				return nil, err
			}
		case "message_action":
			var requestData map[string]interface{}
			if err := hcl.DecodeObject(&requestData, item); err != nil {
				return nil, err
			}
			config = append(config, requestData)
			break
		case "restriction":
			var requestData map[string]interface{}
			if err := hcl.DecodeObject(&requestData, item); err != nil {
				return nil, err
			}
			config = append(config, requestData)
			break
		case "items", "list":
			if err := hcl.DecodeObject(&items_list_Data, item); err != nil {
				return nil, fmt.Errorf(
					"Error reading config for",
					err)
			}
			break
		}
	}
	config = append(config, items_list_Data)
	rawConfig, err := NewRawConfig(config)
	if err != nil {
		return nil, fmt.Errorf(
			"Error reading config form", err)
	}

	return &Resource{
		Name:        resourceName,
		Provider:    providerName,
		Action:      action,
		RequestType: requestType,
		RawConfig:   rawConfig,
	}, nil
}
