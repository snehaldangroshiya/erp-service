package plugin

import (
	"log"
	"net/rpc"

	"github.com/ERP_v4/erp"
	"github.com/hashicorp/go-plugin"
)

// ResourceProviderPlugin is the plugin.Plugin implementation.
type ResourceProviderPlugin struct {
	F func() erp.ResourceProvider
}

func (p *ResourceProviderPlugin) Server(b *plugin.MuxBroker) (interface{}, error) {
	return &ResourceProviderServer{Broker: b, Provider: p.F()}, nil
}

func (p *ResourceProviderPlugin) Client(b *plugin.MuxBroker, c *rpc.Client) (interface{}, error) {
	return &ResourceProvider{Broker: b, Client: c}, nil
}

// ResourceProvider is an implementation of erp.ResourceProvider
// that communicates over RPC.
type ResourceProvider struct {
	Broker *plugin.MuxBroker
	Client *rpc.Client
}

type ResourceProviderItemsArgs struct {
	Resource *erp.Resource
}

func (p *ResourceProvider) Item(r *erp.Resource) *erp.Output {
	log.Println("[Log 5]: Resource provider Items function is called ")
	var resp *erp.Output
	var args = &ResourceProviderItemsArgs{
		Resource: r,
	}

	log.Println("[Log 6]: Triggered RPC server methods")
	p.Client.Call("Plugin.Item", args, &resp)
	log.Println("int ", resp)
	return resp
}

type ResourceProviderListArgs struct {
	Resource *erp.Resource
}

func (p *ResourceProvider) List(r *erp.Resource) *erp.Output {
	log.Println("[Log 5]: Resource provider List function is called ")
	var resp *erp.Output
	var args = &ResourceProviderListArgs{
		Resource: r,
	}

	log.Println("[Log 6]: Triggered RPC server methods")
	p.Client.Call("Plugin.List", args, &resp)
	return resp
}

// ResourceProviderServer is a net/rpc compatible structure for serving
// a ResourceProvider. This should not be used directly.
type ResourceProviderServer struct {
	Broker   *plugin.MuxBroker
	Provider erp.ResourceProvider
}

func (s *ResourceProviderServer) Item(args *ResourceProviderItemsArgs, resp *erp.Output) error {
	log.Println("[Log 7]: Triggered Providers(plugins) function")
	output := s.Provider.Item(args.Resource)
	*resp = *output
	log.Println("Items from ResourceProviderServer")
	return nil
}

func (s *ResourceProviderServer) List(args *ResourceProviderListArgs, resp *erp.Output) error {
	log.Println("[Log 7]: Triggered Providers(plugins) function")
	output := s.Provider.List(args.Resource)
	*resp = *output
	return nil
}
