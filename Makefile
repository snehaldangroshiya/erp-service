BINARY = ERP
GOARCH = amd64

COMMIT=$(shell git rev-parse HEAD)
BRANCH=$(shell git rev-parse --abbrev-ref HEAD)

# Symlink into GOPATH
GITHUB_PROJECT=ERP_v4
BUILD_PROJECT_DIR=github.com/${GITHUB_PROJECT}
CURRENT_DIR=$(shell pwd)

GOBIN=$(GOPATH)/bin
EXE=$(GOBIN)/$(BINARY)-$(COMMIT)
PID=/tmp/go-$(BINARY).pid
OS:=$(shell uname)

run : build
	@$(GOBIN)/$(BINARY)-$(COMMIT) rest-server &

mac : run
	@echo $(PID)
	@lsof -i tcp:8000 > $(PID)

linux : run
	@fuser 8000/tcp > $(PID)

build :
	@go build -i -o $(BINARY)-$(COMMIT) $(BUILD_PROJECT_DIR)
	@mv $(BINARY)-$(COMMIT) $(GOBIN)

clean : stop
	@FILE=$(GOBIN)/$(BINARY)-$(COMMIT)
	@if [ -d $(FILE) ]; then \
		rm -rf $(FILE) || true; \
	fi; \

stop :
	@-kill `[ -f $(PID) ] && cat $(PID)` 2>/dev/null || true
	@if [ -f $(PID) ]; then \
		rm -rf $(PID) || true; \
	fi;\

# synchronize the ldap user data with mongoDB database.
sync-user : build
	@$(GOBIN)/$(BINARY)-$(COMMIT) sync

# Clean the bin dir.
reload : clean run
